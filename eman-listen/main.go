package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	var rabbitmqHost string
	var rabbitmqPort string
	var rabbitmqUser string
	var rabbitmqPassword string
	var rabbitmqExchange string
	var emanDestination string
	flag.StringVar(&rabbitmqHost, "host", "localhost", "RabbitMQ Hostname")
	flag.StringVar(&rabbitmqPort, "port", "5672", "RabbitMQ Port")
	flag.StringVar(&rabbitmqUser, "user", "guest", "RabbitMQ User")
	flag.StringVar(&rabbitmqPassword, "pass", "guest", "RabbitMQ Password")
	flag.StringVar(&rabbitmqExchange, "ex", "eman", "RabbitMQ Exchage")
	flag.StringVar(&emanDestination, "dest", "eman-default", "EMan destination to listen to")
	flag.Parse()

	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", rabbitmqUser, rabbitmqPassword, rabbitmqHost, rabbitmqPort))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		rabbitmqExchange, // name
		"direct",         // type
		true,             // durable
		false,            // auto-deleted
		false,            // internal
		false,            // no-wait
		nil,              // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,           // queue name
		emanDestination,  // routing key
		rabbitmqExchange, // exchange
		false,
		nil,
	)
	failOnError(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for events. To exit press CTRL+C")
	<-forever
}
