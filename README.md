# eman

![[Pipeline Status](https://gitlab.com/mborrajo/eman-test/)]( https://gitlab.com/mborrajo/eman-test/badges/master/pipeline.svg )

![[Quality Gate Status](http://djbassboss.ddns.net:19000/api/project_badges/measure?project=mborrajo_eman-test_AXxiPJHhy8Ji91WTykaO&metric=alert_status)](http://djbassboss.ddns.net:19000/dashboard?id=mborrajo_eman-test_AXxiPJHhy8Ji91WTykaO)

### **This software is in early alpha and under heavy development. Things are likely to change** 

Eman is an event/alert manager, with it you can centralize all your event sources, modify them and send them to other Eman instances or other integrations.

It uses [RabbitMQ](https://www.rabbitmq.com/) for messaging between managers and integrations and [ArangoDB](https://www.arangodb.com/) for event storage.

Rules for processing events are defined in javascript. Right now it uses the [otto](https://github.com/robertkrimen/otto) js interpreter for golang.

## Event Flow

```mermaid
graph LR
    subgraph Event Processing
        ev[Rule Engine]
        db[DB Engine]
        ev --- db
    end
    subgraph RabbitMQ Exchange
        amqpin[Input Topic]
        amqpout[Output Topic]
    end
    subgraph Database
        arangodb[(ArangoDB)]
    end

    amqpin --> ev
    db --- arangodb
    ev --> amqpout

```
## JS Folder structure

Rules are organized into folders:
 - defines: This folder includes js files that will run at startup. Its intended to declare custom js functions.
 - rules: This folder includes js files with code that will be run for each new event. Rule files will be executed in alphabetical order.

## JS Api for rules

The current event being processed is exposed to the code as $this and has the following fields:
```golang
type Event struct {
	ID             string
	Key            string `json:"_key"`
	Message        string
	Severity       string
	Status         string
	Host           string
	HostGroup      string
	MonitorGroup   string
	MonitorClass   string
	Instance       string
	Metric         string
	MetricID       string
	MetricValue    float64
	Origin         string
	ReceptionTime  time.Time
	OriginTime     time.Time
	AckTime        time.Time
	ControlledTime time.Time
	EndTime        time.Time
	Propagations   string
	Repetitions    int
	Drop           bool
}
```
Setting Drop to true will discard the event at the end of the execution of the current rule.

## Example rules

```js
// Rule that set a generic message if the Message is empty
if  ($this.message == "") {
	$this.Message = "Alert from host " + $this.Host
		+ ": " + $this.MonitorClass
		+ "  " + $this.Instance
		+ " with metric  "  + $this.Metric
		+ " is at " + $this.MetricValue
}
```

```js
// Rule for filtering duplicate events and updating the first event MetricValue, Message, Severity and Status.
cond = [
	"ev.Host == '" + $this.Host + "'",
	"ev.HostGroup == '" + $this.HostGroup + "'",
	"ev.MonitorGroup == '" + $this.MonitorGroup + "'",
	"ev.MonitorClass == '" + $this.MonitorClass + "'",
	"ev.Instance == '" + $this.Instance + "'",
	"ev.Metric == '" + $this.Metric + "'",
	"ev.Origin == '" + $this.Origin + "'",
	"ev.Status != 'CLOSED' "
].join(" AND  ")

FindEvents(cond).forEach(function(e){
	console.log(JSON.stringify(e, null, 2))
	e.Repetitions += 1
	e.MetricValue = $this.MetricValue
	e.Message = $this.Message
	e.Severity = $this.Severity
	e.Status = $this.Status
	UpdateEvent(e)
	$this.Drop = true
})
```

```js
// Rule that send an this event to an integration called Speach
SendEvent("Speach", $this)
```

## To do List:
[ ] - Propagations (Forward and backwards)  
[ ] - Tests   
[ ] - Custom Fields  
[ ] - Hot reload of rules  
[ ] - Improve Documentation  
[ ] - Custom prometheus metrics  
[ ] - Kapacitor integration   
[ ] - Grafana datasource   
[ ] - Nagios Custom Notification example  