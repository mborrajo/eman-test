module gitlab.com/mborrajo/eman

go 1.17

require (
	github.com/arangodb/go-driver v1.2.1
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/mattn/go-colorable v0.1.12
	github.com/prometheus/client_golang v1.11.0
	github.com/robertkrimen/otto v0.0.0-20211024170158-b87d35c0b86f
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
)

require (
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.26.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	google.golang.org/protobuf v1.26.0-rc.1 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
