package common

import (
	"log"
	"strings"
)

func CheckError(module string, op string, e error) bool {
	if e != nil {
		if op != "" {
			log.Printf("%s - %s: '%s'", module, op, e)
		} else {
			log.Printf("%s: '%s'", module, e)
		}
		return true
	}
	return false
}

func CheckErrorPanic(module string, op string, e error) {
	if CheckError(module, op, e) {
		log.Panic("Fatal Error")
	}
}

func LogDebug(module string, op string, message string) {
	lines := strings.Split(message, "\n")
	for _, l := range lines {
		log.Printf("DEBUG - %s - %s: '%s'", module, op, l)
	}
}
