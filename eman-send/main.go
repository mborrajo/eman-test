package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/streadway/amqp"
	e "gitlab.com/mborrajo/eman/common"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	var rabbitmqHost string
	var rabbitmqPort string
	var rabbitmqUser string
	var rabbitmqPassword string
	var rabbitmqExchange string
	var emanDestination string

	flag.StringVar(&rabbitmqHost, "host", "localhost", "RabbitMQ Hostname")
	flag.StringVar(&rabbitmqPort, "port", "5672", "RabbitMQ Port")
	flag.StringVar(&rabbitmqUser, "user", "guest", "RabbitMQ User")
	flag.StringVar(&rabbitmqPassword, "pass", "guest", "RabbitMQ Password")
	flag.StringVar(&rabbitmqExchange, "ex", "eman", "RabbitMQ Exchage")
	flag.StringVar(&emanDestination, "dest", "eman-default", "Destination EMan")

	var eMessage string
	var eSeverity string
	var eStatus string
	var eHost string
	var eHostGroup string
	var eMonitorGroup string
	var eMonitorClass string
	var eInstance string
	var eMetric string
	var eMetricID string
	var eMetricValue float64
	var eOrigin string
	hostname, _ := os.Hostname()
	flag.StringVar(&eSeverity, "eSev", "INFO", "Event severity")
	flag.StringVar(&eStatus, "eStatus", "OPEN", "Event status")
	flag.StringVar(&eHost, "eHost", hostname, "Event Hostname")
	flag.StringVar(&eHostGroup, "eHostGrp", "eman-send", "Event host group")
	flag.StringVar(&eMonitorGroup, "eMonGrp", "eman-send", "Event monitor group")
	flag.StringVar(&eMonitorClass, "eMonClass", "eman-send", "Event monitor class")
	flag.StringVar(&eInstance, "eInst", "eman-send", "Event instance")
	flag.StringVar(&eMetric, "eMet", "eman-send", "Event metric")
	flag.StringVar(&eMetricID, "eMetId", "eman-send", "Event metric Id")
	flag.Float64Var(&eMetricValue, "eMetVal", 0, "Event metric value")
	flag.StringVar(&eOrigin, "eOrigin", "eman-send", "Event origin")
	flag.StringVar(&eMessage, "eMsg", fmt.Sprintf("eman-send event from %s", hostname), "Event message")

	flag.Parse()

	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", rabbitmqUser, rabbitmqPassword, rabbitmqHost, rabbitmqPort))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()
	ev := e.NewEvent()
	ev.Message = eMessage
	ev.Severity = eSeverity
	ev.Status = eStatus
	ev.Host = eHost
	ev.HostGroup = eHostGroup
	ev.MonitorGroup = eMonitorGroup
	ev.MonitorClass = eMonitorClass
	ev.Instance = eInstance
	ev.Metric = eMetric
	ev.MetricID = eMetricID
	ev.MetricValue = eMetricValue
	ev.Origin = eOrigin
	body := ev.ToPrettyString()
	err = ch.Publish(
		rabbitmqExchange, // exchange
		emanDestination,  // routing key
		false,            // mandatory
		false,            // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
}
